add_definitions(${QT_DEFINITIONS} ${KDE4_DEFINITIONS})
include_directories(${KDE4_INCLUDES} ${QT_INCLUDES})

set(kded_imchooserstarter_PART_SRCS  imchooserstarter.cpp )

kde4_add_plugin(kded_imchooserstarter  ${kded_imchooserstarter_PART_SRCS})

target_link_libraries(kded_imchooserstarter  ${KDE4_KIO_LIBS})

install(TARGETS kded_imchooserstarter  DESTINATION ${PLUGIN_INSTALL_DIR} )
install( FILES imchooserstarter.desktop  DESTINATION  ${SERVICES_INSTALL_DIR}/kded )